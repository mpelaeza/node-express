let request = require('request');
let server = require('../../bin/www')
var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta')

let base_url = 'http://localhost:3000'

afterEach(function(done) {
	Bicicleta.deleteMany({}, function(err, success) {
		if (err) console.log(err);
		done()
	})
})

describe('Bicicletas API', function(){

	describe('Get BICICLETAS /', () => {
		it(' Status 200', (done) => {
			Bicicleta.allBicis((err, bicis) => {
       if (err)	console.log(err)
		    expect(bicis.length).toBe(0)	
			})
				let a = Bicicleta.createInstance(1, 'rojo', 'urbana', [6.2361854,-75.5824796])
				Bicicleta.add(a, () => {
					request.get(base_url + '/api/bicicletas', function(error, response, body){
							expect(response.statusCode).toBe(200)
							done()
					})
				});
		})
	})


	describe('POST BICICLETAS /create', () => {
		it(' Status 200', (done) => {
			Bicicleta.allBicis((err, bicis) => {
				if (err)	console.log(err)
				expect(bicis.length).toBe(0)	
			})
			let headers = {'content-type': 'application.json'};
			let aBici = {"code": 1, "color": "azul", "modelo": "urbana", "lat": "6.2361854", "lng": "-75.5824796"}
			request.post({
					headers: headers,
					uri: 'http://localhost:3000/api/bicicletas/create',
					form:aBici
			}, function(error, response, body){
					expect(response.statusCode).toBe(200)
					Bicicleta.findByCode(1, (err, bici) => {
						if (err) console.log(err) 
						expect(bici.color).toBe("azul")
						done()
					})
				})
		})
	})


	describe('POST BICICLETAS /destroy', () => {
		it(' Status 204', (done) => {
				let headers = {'content-type': 'application.json'};
				let aBici = {"code": 1, "color": "rojo", "modelo": "urbana", "lat": "6.2361854", "lng": "-75.5824796"}
				Bicicleta.add(aBici, (err, bici) => {
					request.post({
							headers: headers,
							uri: 'http://localhost:3000/api/bicicletas/destroy',
							form: {id: 1}
					}, function(error, response, body){
							expect(response.statusCode).toBe(204)
							Bicicleta.allBicis((err, bicis) => {
								if (err)	console.log(err)
								expect(bicis.length).toBe(0)	
								done()
							})
					})
				})
		})
	})
	

	describe('POST BICICLETAS /:id/update', () => {
		it(' Status 200', (done) => {
			let headers = {'content-type': 'application.json'};
			let aBici = {"code": 1, "color": "rojo", "modelo": "urbana", "lat": "6.2361854", "lng": "-75.5824796"}
			Bicicleta.add(aBici, (err, bici) => {
				
				let aBiciNew = {"color": "verde"}
				request.post({
						headers: headers,
						uri: 'http://localhost:3000/api/bicicletas/1/update',
						form: aBiciNew
				}, function(error, response, body){
						expect(response.statusCode).toBe(200)

						Bicicleta.findByCode(1, (err, bici) => {
							if (err) console.log(err) 
							expect(bici.color).toBe("verde")
							done()
						})
				})
			})
		})
	})
})


	/*
	describe('POST BICICLETAS /:id/update', () => {
		it(' Status 200', (done) => {
				let headers = {'content-type': 'application.json'};
				let aBici = {"id": 1, "color": "rojo", "modelo": "urbana", "lat": "6.2361854", "lng": "-75.5824796"}
				Bicicleta.create(aBici)
				let aBiciNew = {"id": 1, "color": "verde", "modelo": "urbana", "lat": "6.2361854", "lng": "-75.5824796"}
				request.post({
						headers: headers,
						uri: 'http://localhost:3000/api/bicicletas/1/update',
						form: aBiciNew
				}, function(error, response, body){
						expect(response.statusCode).toBe(200)
						expect(Bicicleta.findById(1).color).toBe('verde')
						done()
				})
		})
	})
	});
	*/
