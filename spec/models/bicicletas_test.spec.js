var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta')

describe('Testing Bicicletas', function(){
/*	beforeEach(function() {
		var mongoDb  = 'mongodb://localhost/testdb';
		mongoose.connect(mongoDb, { useNewUrlParser: true, useUnifiedTopology: true})

		const db = mongoose.connection;
		db.on('error', console.error.bind(console, 'connection error'))
		db.once('open', function() {
			console.log('Conectados a la base de datos');
		})

	})*/ 

	afterEach(function(done) {
		Bicicleta.deleteMany({}, function(err, success) {
			if (err) console.log(err);
			done()
		})
	})

	describe( 'createInstance', () => {
		it('crea una instancia de bicicleta', () => {
			var bici = Bicicleta.createInstance(1, "verde", "urbana", [6.2361854,-75.5824796])
			expect(bici.code).toBe(1);
			expect(bici.color).toBe("verde");
			expect(bici.modelo).toBe("urbana");
			expect(bici.ubicacion[0]).toBe(6.2361854)
			expect(bici.ubicacion[1]).toBe(-75.5824796)

		})
	})

	describe('Bicicleta.allBicis', () => {
		it('comienza vacia', (done) => {
			Bicicleta.allBicis(function(err, bicis) {
				expect(bicis.length).toBe(0)
				done()
			})
		})
	})


	describe('Bicicleta.add', () => {
		it('agregar solo una bici', (done) => {
			var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"})
			Bicicleta.add(aBici, function(err, newBici) {
				if (err) console.log(err);
				Bicicleta.allBicis(function(err, bicis) {
			  	expect(bicis.length).toBe(1)
			  	expect(bicis[0].code).toBe(aBici.code)
					done()
				})
			})
		})
	})


	describe('Bicicleta.findByCode', () => {
		it('Buscar una bici por codigo', (done) => {
			Bicicleta.allBicis(function(err, bicis) {
				expect(bicis.length).toBe(0)
				var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"})
				Bicicleta.add(aBici, function(err, newBici) {
					if (err) console.log(err)
					var aBici2 = new Bicicleta({code: 2, color: "rojo", modelo: "montaña"})
				  Bicicleta.add(aBici2, function(err, newBici2) {
					  if (err) console.log(err)
						Bicicleta.findByCode(1, (err, targetBici) => {
							expect(targetBici.code).toBe(1)
							expect(targetBici.color).toBe("verde")
							expect(targetBici.modelo).toBe("urbana")
							done() 
						})
					})
				})
			})

		})
	})


	describe('Bicicleta.removeByCode', () => {
		it('Borrar una bici por codigo', (done) => {
			Bicicleta.allBicis(function(err, bicis) {
				expect(bicis.length).toBe(0)
				var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"})
				Bicicleta.add(aBici, function(err, newBici) {
					if (err) console.log(err)
					var aBici2 = new Bicicleta({code: 2, color: "rojo", modelo: "montaña"})
				  Bicicleta.add(aBici2, function(err, newBici2) {
					  if (err) console.log(err)
						Bicicleta.removeByCode(1, (err, targetBici) => {
							Bicicleta.allBicis(function(err, bicis) {
								expect(bicis.length).toBe(1)
								expect(bicis[0].code).toBe(2)
								expect(bicis[0].color).toBe("rojo")
								expect(bicis[0].modelo).toBe("montaña")
								done()
							})
						})
					})
				})
			})
		 })
	})


	describe('Bicicleta.update', () => {
		it('Actualizar una bici por codigo', (done) => {
			Bicicleta.allBicis(function(err, bicis) {
				expect(bicis.length).toBe(0)
				var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"})
				Bicicleta.add(aBici, function(err, newBici) {
					if (err) console.log(err)
					var aBici2 = new Bicicleta({code: 2, color: "rojo", modelo: "montaña"})
				  Bicicleta.add(aBici2, function(err, newBici2) {
					  if (err) console.log(err)
						Bicicleta.update(1,{color: "dorado"}, (err, targetBici) => {
							Bicicleta.findByCode(1, function(err, bici) {
								expect(bici.code).toBe(1)
								expect(bici.color).toBe("dorado")
								expect(bici.modelo).toBe("urbana")
								done()
							})
						})
					})
				})
			})
		 })
	 })
})






/*
beforeEach(() => Bicicleta.allBicis = [])

describe('Bicicleta.allBicics', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
    })
})

describe('Bicicleta.add', () => {
    it('Agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
        let a = new Bicicleta(1, 'rojo', 'urbana', [6.2361854,-75.5824796])
        Bicicleta.add(a);
        expect(Bicicleta.allBicis[0]).toBe(a)
        expect(Bicicleta.allBicis.length).toBe(1)
    })
})

describe('Bicicleta.findById', () => {
    it('Debe devolver la Bici con Id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
        let a = new Bicicleta(1, 'rojo', 'urbana', [6.2361854,-75.5824796])
        let b = new Bicicleta(2, 'verde', 'urbana', [6.2361854,-75.5824796])
        Bicicleta.add(a);
        Bicicleta.add(b);
        expect(Bicicleta.findById(1)).toBe(a);
    })
})

describe('Bicicleta.removeById', () => {
    it('Debe borrar la Bici con Id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
        let a = new Bicicleta(1, 'rojo', 'urbana', [6.2361854,-75.5824796])
        let b = new Bicicleta(2, 'verde', 'urbana', [6.2361854,-75.5824796])
        Bicicleta.add(a);
        Bicicleta.add(b);
        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(1)
    })
})*/
