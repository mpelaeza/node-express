var mymap = L.map('main-map').setView([6.2466994, -75.5771018], 12);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
}).addTo(mymap);

var marker = L.marker([6.2466994, -75.5771018]).addTo(mymap);
var marker = L.marker([6.239159,-75.5767039]).addTo(mymap);
var marker = L.marker([6.2361854,-75.5824796]).addTo(mymap);

var biciIcon = L.icon({
    iconUrl: 'images/bike.png',
    iconSize:     [50, 50], // size of the icon
    shadowSize:   [50, 50], // size of the shadow
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
})

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        result.bicicletas.forEach(function(bici){
            var marker = L.marker(bici.ubicacion, {icon: biciIcon, title: bici.id}).addTo(mymap);
        })
    }
})
