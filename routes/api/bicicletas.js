var express = require('express');
var router = express.Router();
var apiBicicletaController = require('../../controllers/api/bicicletaController');

router.get('/', apiBicicletaController.bicicleta_list)
router.post('/create', apiBicicletaController.bicicleta_create)
router.post('/destroy', apiBicicletaController.bicicleta_destroy)
router.post('/:id/update', apiBicicletaController.bicicleta_update)


module.exports = router;
