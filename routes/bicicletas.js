var express = require('express');
var router = express.Router();
var bicicletaController = require('../controllers/bicicleta');

router.get('/', bicicletaController.bicicleta_list);
router.get('/create', bicicletaController.bicicleta_new);
router.post('/create', bicicletaController.bicicleta_create);
router.get('/:id/edit', bicicletaController.bicicleta_edit);
router.get('/:id', bicicletaController.bicicleta_show);
router.post('/:id', bicicletaController.bicicleta_update);
router.post('/:id/destroy', bicicletaController.bicicleta_destroy);

module.exports = router;
