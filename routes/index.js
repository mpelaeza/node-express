var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/express', function(req, res){
  res.render( 'express-welcome', {title: 'Express'})
})

module.exports = router;
