# RED BICICLETAS

Projecto en Nodejs + Express para red de bicicletas

## Link
https://bitbucket.org/mpelaeza/node-express/src/master/

## Comenzando 🚀
1. Crea una carpeta y dirigete a ella
2. Clona el repo en tu local  
    ```git clone git@bitbucket.org:mpelaeza/node-express.git```
    
3. Instala las dependencias ```npm install```
4. Para correr en dev utiliza ```npm run devstart```
    

