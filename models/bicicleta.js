var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
	code: Number, 
	color: String,
	modelo: String,
	ubicacion: {
		type: [Number], index: {type: '2disphere', sparse: true}
	}
})

bicicletaSchema.methods.toString = function() {
	return 'code: ' + this.code + ' | color: ' + this.color;
};

bicicletaSchema.statics.allBicis = function(cb) {
	return this.find({}, cb);
};

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
	return new this({
		code: code,
		color: color,
		modelo: modelo,
		ubicacion: ubicacion
	});
}; 

bicicletaSchema.statics.findByCode = function(code, cb) {
	return this.findOne({code: code}, cb);
};

bicicletaSchema.statics.add = function(aBici, cb) {
	this.create(aBici, cb)
}

bicicletaSchema.statics.removeByCode = function(code, cb) {
	this.deleteOne({code: code}, cb)
}

bicicletaSchema.statics.update = function(code,params, cb) {
	this.updateOne({code: code}, params, cb);
}
module.exports =  mongoose.model('Bicicleta', bicicletaSchema);




//Bicicleta.update = function(id, params){
 //   let bici = Bicicleta.findById(id)
//
//    bici.id =  params.id
//    bici.color =  params.color
//    bici.modelo =  params.modelo
//    bici.ubicacion = [params.lat, params.lng]
//
//    return bici
//}



// let a = new Bicicleta(1, 'rojo', 'urbana', [6.2361854,-75.5824796])
// let b = new Bicicleta(2, 'azul', 'urbana', [6.239159,-75.5767039])

// Bicicleta.add(a);
// Bicicleta.add(b);

// module.exports = Bicicleta
