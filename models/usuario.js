let mongoose = require('mongoose');
// let Reserva = require('./reserva');
let Schema = mongoose.Schema
let bcrypt = require('bcrypt')
const uniqueValidator = require('mongoose-unique-validator')


const saltRounds = 10

const validateEmail = function(email) {
	const re = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g
	return re.test(email);
}


var usuarioSchema = new Schema({
	nombre: {
		type: String,
		trim: true,
		required: [true, "El nombre es obligatorio"]
	},
	email: {
		type: String,
		trim: true,
		required: [true, "El email es obligatorio"],
		lowercase: true,
		unique: true,  
		validate: [validateEmail, 'Por favor, ingrese un email valido'],
		match: [/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/]
	},
	password: {
		type: String,
		required: [true, "El password es obligatorio"]
	},
	passwordResetToken: String,
	passwordResetTokenExpires: Date,
	verificado: {
		type: Boolean,
		default: false
	}
});

usuarioSchema.plugin(uniqueValidator, {message: "El {PATH} ya existe con otro usuario"})

usuarioSchema.pre('save', function(next) {
	if (this.isModified('password')){
		this.password = bcrypt.hashSync(this.password, saltRounds)
	}
})

usuarioSchema.methods.validPassword = function(password){
	return bcrypt.compareSync(password, this.password)
}
/*UsuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
	let reserva = new Reserva({usuario: this, bicicleta: biciId, desde: desde, 
														 hasta: hasta})
	console.log(reserva)
	reserva.save(cb)
}*/

module.exports = mongoose.model('Usuario', UsuarioSchema)
