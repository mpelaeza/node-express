var Bicicleta = require('../../models/bicicleta')

exports.bicicleta_list = function (req, res) {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    })
}

exports.bicicleta_create = function(req, res){
    let bici = Bicicleta.add(req.body, (err, bici) => {
     if (err) res.status(500).json({error: err})
			res.status(200).json({
					bicicleta: bici
			})
		})
}

exports.bicicleta_update = function (req, res) {
    let id = req.params.id
    let bici = Bicicleta.update(id, req.body, (err, bici) => {
			if (err) res.status(500).json({error: err})
			res.status(200).json({
					bicicleta: bici
			})
		})
}

exports.bicicleta_destroy = function (req, res) {
    Bicicleta.removeByCode(req.body.id, (err, bici) => {
			if (err) res.status(500).json({error: err})
			res.status(204).send()
		});

}
