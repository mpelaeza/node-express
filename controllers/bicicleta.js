var Bicicleta = require('../models/bicicleta')

exports.bicicleta_list = function(req, res){
    res.render('bicicletas/index', {bicis: Bicicleta.allBicis})
}

exports.bicicleta_show = function(req, res){
    let id = req.params.id
    res.render('bicicletas/show', {bici: Bicicleta.findById(id)})
}

exports.bicicleta_new = function(req, res){
    res.render('bicicletas/create')
}

exports.bicicleta_create = function(req, res){
    Bicicleta.create(req.body)
    res.redirect('/bicicletas')
}

exports.bicicleta_edit = function(req, res){
    let id = req.params.id
    res.render('bicicletas/edit', {bici: Bicicleta.findById(id)})
}

exports.bicicleta_update = function (req, res) {
    let id = req.params.id
    Bicicleta.update(id, req.body)

    res.redirect('/bicicletas')
}

exports.bicicleta_destroy = function (req, res) {
    Bicicleta.removeById(req.body.id);

    res.redirect('/bicicletas')
}

